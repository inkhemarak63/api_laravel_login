<?php

namespace App\Http\Middleware;

use App\Posts;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
class AdminAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guard('api')->check() && $request->user()->type >= 1) {
            return $next($request);
        } else {
            $message = ["message" => "aa Permission Denied"];
            return response($message, 401);
        }
    }
}

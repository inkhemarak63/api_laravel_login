<!DOCTYPE html>
<html>
  <head>
    <title>Server Maintenance UI in HTML/CSS</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Teko" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <style type="text/css">
      body{
      font-family: 'Teko', sans-serif;
      background-color: #f2f2f2;
      margin: 0px;
      }
      section{

      text-align: center;
      height: 40vh;
      width: 100%;
      display: flex;
      align-items: center;
      justify-content: center;
      flex-direction: column;

      }
      h2,p{
      color: #3867d6;
      margin: 0px;
      }
      h2{
      font-size: 62px;
      padding-top: 20px;
      }
      p{
      font-size: 40px;
      padding-bottom: 20px;
      }
      img{
      max-width: 100%;
      }
      a{
      background: #3867d6;
      border-radius:4px;
      outline: none;
      border: 0px;
      color: #fff;
      font-size: 34px;
      cursor: pointer;
      text-decoration: none;
      padding: 5px 25px;
      }
      a:hover{
      background-color: #1d56de;
      }
      @media(max-width: 625px){
      h2{
      font-size: 50px;
      }
      p{
      font-size: 30px;
    
      }
      }
      @media(max-width: 492px){
      h2{
      font-size: 30px;
      }
      a{
      	font-size: 25px;
      }
      p{
      font-size: 25px;
        line-height: 26px;
      }
      }

  @import url(//fonts.googleapis.com/css?family=Montserrat:300,500);
.team4 {
  font-family: "Montserrat", sans-serif;
	color: #8d97ad;
  font-weight: 300;
}

.team4 h1, .team4 h2, .team4 h3, .team4 h4, .team4 h5, .team4 h6 {
  color: #3e4555;
}

.team4 .font-weight-medium {
	font-weight: 500;
}

.team4 h5 {
    line-height: 22px;
    font-size: 18px;
}

.team4 .subtitle {
    color: #8d97ad;
    line-height: 24px;
		font-size: 13px;
}

.team4 ul li a {
  color: #8d97ad;
  padding-right: 15px;
  -webkit-transition: 0.1s ease-in;
  -o-transition: 0.1s ease-in;
  transition: 0.1s ease-in;
}

.team4 ul li a:hover {
  -webkit-transform: translate3d(0px, -5px, 0px);
  transform: translate3d(0px, -5px, 0px);
	color: #316ce8;
}
    </style>
  </head>
  <body>
    <section>
     
      <h2>សុំទោស! ទីនេះមិនមានអ្វីអោយអ្នកទេ</h2>
      <p>ខណះពេលនេះ Developer យើងកំពុងជាប់រៀន។ ចាំចូលម្តងទៀតនៅពេលក្រោយ</p>
      <h1>React + Laravel = Mobile legends</h1>
      ចុចទៅ<a href="#">ផ្ទះ</a>
      
      <!-- <img src="Maintenance.svg"> -->
    </section>
  <hr>
    <div class="py-5 team4">
  <div class="container">
    <div class="row justify-content-center mb-4">
      <div class="col-md-7 text-center">
        <h3 class="mb-3">ក្រុមគ្មានបទពិសោធន៍ និងវិជ្ជាជីវៈ</h3>
        <h6 class="subtitle">អ្នកអាចបញ្ជូនបន្តនៅលើបញ្ជីលក្ខណៈពិសេសដ៏អស្ចារ្យរបស់យើង ហើយសេវាកម្មអតិថិជនរបស់យើងនឹងក្លាយជាបទពិសោធន៍ដ៏អស្ចារ្យសម្រាប់អ្នកដោយគ្មានការសង្ស័យ និងគ្មានពេលវេលា</h6>
      </div>
    </div>
    <div class="row">
      <!-- column  -->
      <div class="col-lg-3 mb-4">
        <!-- Row -->
        <div class="row">
          <div class="col-md-12">
            <img src="https://i.imgur.com/hQVsTlm.jpg" alt="wrapkit" class="img-fluid rounded-circle" />
          </div>
          <div class="col-md-12 text-center">
            <div class="pt-2">
              <h5 class="mt-4 font-weight-medium mb-0">KING ERROR</h5>
              <h6 class="subtitle mb-3">Property Specialist</h6>
            </div>
          </div>
        </div>
        <!-- Row -->
      </div>
      <!-- column  -->
      <!-- column  -->
      <div class="col-lg-3 mb-4">
        <!-- Row -->
        <div class="row">
          <div class="col-md-12">
            <img src="https://i.imgur.com/i2WSPn7.jpg" alt="wrapkit" class="img-fluid rounded-circle" />
          </div>
          <div class="col-md-12 text-center">
            <div class="pt-2">
              <h5 class="mt-4 font-weight-medium mb-0">KING UNDO</h5>
              <h6 class="subtitle mb-3">Property Specialist</h6>
            </div>
          </div>
        </div>
        <!-- Row -->
      </div>
      <!-- column  -->
      <!-- column  -->
      <div class="col-lg-3 mb-4">
        <!-- Row -->
        <div class="row">
          <div class="col-md-12">
            <img src="https://i.imgur.com/IwlvFik.jpg" alt="wrapkit" class="img-fluid rounded-circle" />
          </div>
          <div class="col-md-12 text-center">
            <div class="pt-2">
              <h5 class="mt-4 font-weight-medium mb-0">KING COPY</h5>
              <h6 class="subtitle mb-3">Property Specialist</h6>
            </div>
          </div>
        </div>
        <!-- Row -->
      </div>
      <!-- column  -->
      <!-- column  -->
      <div class="col-lg-3 mb-4">
        <!-- Row -->
        <div class="row">
          <div class="col-md-12">
            <img src="https://i.imgur.com/YbRCNiO.jpg" alt="wrapkit" class="img-fluid rounded-circle" />
          </div>
          <div class="col-md-12 text-center">
            <div class="pt-2">
              <h5 class="mt-4 font-weight-medium mb-0">KING CODE</h5>
              <h6 class="subtitle mb-3">Property Specialist</h6>
            </div>
          </div>
        </div>
        <!-- Row -->
      </div>
    </div>
  </div>
</div>
  </body>
</html>